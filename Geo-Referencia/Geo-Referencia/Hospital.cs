﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geo_Referencia
{
    class Hospital
    {
        private string nombre;
        private int tipo;
        private int calle;
        private int avenida;

        public Hospital(string nombre, int tipo, int calle, int avenida) {
            this.nombre = nombre;
            this.tipo = tipo;
            this.calle = calle;
            this.avenida = avenida;
        }

        public string getNombre() {
            return nombre;
        }

        public int getTipo() {
            return tipo;
        }

        public int getCalle() {
            return calle;
        }

        public int getAvenida() {
            return avenida;
        }
    }
}
