﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geo_Referencia
{
    class Vehiculo
    {
        private string placa;
        private string marca;
        private int velocidad;
        private int calle;
        private int avenida;

        public Vehiculo(string placa, string marca, int velocidad, int calle, int avenida) {
            this.placa = placa;
            this.marca = marca;
            this.velocidad = velocidad;
            this.calle = calle;
            this.avenida = avenida;
        }

        public void setCalle(int calle) {
            this.calle = calle;
        }

        public void setAvenida(int avenida) {
            this.avenida = avenida;
        }

        public string getPlaca() {
            return placa;
        }

        public string getMarca() {
            return marca;
        }

        public int getVelocidad() {
            return velocidad;
        }

        public int getCalle() {
            return calle;
        }

        public int getAvenida() {
            return avenida;
        }
    }
}
