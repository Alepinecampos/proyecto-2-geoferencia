﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geo_Referencia
{
    class Gasolinera
    {
        private string nombre;
        private double precio;
        private int calle;
        private int avenida;

        public Gasolinera(string nombre, double precio, int calle, int avenida) {
            this.nombre = nombre;
            this.precio = precio;
            this.calle = calle;
            this.avenida = avenida;
        }

        public string getNombre() {
            return nombre;
        }

        public double getPrecio() {
            return precio;
        }

        public int getCalle() {
            return calle;
        }

        public int getAvenida() {
            return avenida;
        }
    }
}
