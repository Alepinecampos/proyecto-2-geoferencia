﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Geo_Referencia
{
    public partial class Form1 : Form
    {

        private bool presionado;
        private bool ruta;
        private int x;
        private int y;
        private string path;
        private Municipalidad alcaldia;
        private Label muni;
        private Object[,] mapa, objetos;
        private Vehiculo[] autos;
        private Object[] vehiculos;
        private Restaurante[] restaurantes;
        private Gasolinera[] gasolinera;
        private Policia[] policia;
        private Bombero[] bombero;
        private Hospital[] hospital;

        public Form1()
        {
            InitializeComponent();

            path = "D:\\Mis Documentos\\Mis Documentos Nuevos\\Universidad\\Programación\\Intro. a Progra\\Box Sync\\Proyecto 2\\Proyecto Respaldo\\Geo-Referencia\\Geo-Referencia\\Resources";
            ruta = false;
            alcaldia = new Municipalidad();
            mapa = new Object[30, 30];
            objetos = new Object[30, 30];
            autos = new Vehiculo[100];
            vehiculos = new Object[100];
            restaurantes = new Restaurante[50];
            gasolinera = new Gasolinera[50];
            policia = new Policia[20];
            bombero = new Bombero[20];
            hospital = new Hospital[20];
            muni = new Label();
            muni.BackColor = Color.Transparent;
            muni.Image = Image.FromFile(path + "City-Hall.png");
            muni.SetBounds((label1.Bounds.Location.X + 27 * alcaldia.getCalle() + 50 * (alcaldia.getCalle() - 1)), 
                            (label1.Bounds.Location.Y + 30 * alcaldia.getAvenida() + 50 * (alcaldia.getAvenida() - 1)), 50, 50);
            muni.Click += mostrarInfo;
            panel1.Controls.Add(muni);
            mapa[alcaldia.getAvenida()-1, alcaldia.getCalle()-1] = muni;
            objetos[alcaldia.getAvenida() - 1, alcaldia.getCalle() - 1] = alcaldia;
            lecturaArchivo();
        }

        private void refrescar() {
            label1.SendToBack();
            label1.Refresh();
        }

        private void paint(object sender, PaintEventArgs e)
        {
            if (ruta) {
                Pen blackPen = new Pen(Color.White, 25);
                Point point1 = new Point(x, y);
                Point point2 = new Point(x, y + 55);

                e.Graphics.DrawLine(blackPen, point1, point2);
                point1 = new Point(x - 12, y + 65);
                point2 = new Point(x + 50, y + 65);
                e.Graphics.DrawLine(blackPen, point1, point2);
                label1.Refresh();
                blackPen.Dispose();
            }
        }
  
        private void mouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                presionado = true;
                x = e.X;
                y = e.Y;
            }
        }

        private void mouseMove(object sender, MouseEventArgs e)
        {
            if (presionado)
            {
                label1.Left += e.X - x;
                label1.Top += e.Y - y;
                for (int fila = 0; fila < 30; fila++) {
                    for (int columna = 0; columna < 30; columna++) {
                        if (mapa[fila, columna] != null) {
                            ((Label)mapa[fila, columna]).Left += e.X - x;
                            ((Label)mapa[fila, columna]).Top += e.Y - y;
                        }
                    }
                }
                for (int i = 0; i < 100; i++) {
                    if (autos[i] != null) {
                        ((Label)vehiculos[i]).Left += e.X - x;
                        ((Label)vehiculos[i]).Top += e.Y - y;
                    }
                }
            }
        }

        private void mouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                presionado = false;
            }
        }

        private void lecturaArchivo() {
            String cadena = "";
            int referencia = 0;
            StreamReader file = new StreamReader(path + "datos.txt");
            while ((cadena = file.ReadLine()) != null) {
                if (cadena.Equals("VEHICULOS")) {
                    referencia = 1;
                }else if(cadena.Equals("RESTAURANTES")){
                    referencia = 2;
                }else if(cadena.Equals("HOSPITALES")){
                    referencia = 3;
                }else if(cadena.Equals("GASOLINERAS")){
                    referencia = 4;
                }else if(cadena.Equals("POLICIA")){
                    referencia = 5;
                }else if(cadena.Equals("BOMBERO")){
                    referencia = 6;
                }else{
                    agregar(referencia, cadena);
                }
            }
            refrescar();
        }

        private void agregar(int referencia, string cadena) {
            Label label = new Label();
            label.BackColor = Color.Transparent;
            char[] parametros = {'/'};
            string[] datos = cadena.Split(parametros);
            bool bandera = true;

            switch (referencia) {
                case 1:
                    Vehiculo v = new Vehiculo(datos[0], datos[1], Convert.ToInt32(datos[2]), Convert.ToInt32(datos[3]), Convert.ToInt32(datos[4]));
                    for (int i = 0; i < 100; i++) {
                        if (autos[i] != null && autos[i].getAvenida() == v.getAvenida() && autos[i].getCalle() == v.getCalle())
                        {
                            bandera = false;
                        }
                    }
                    if (bandera) {
                        label.Image = Image.FromFile(path + "Car.png");
                        label.SetBounds((label1.Bounds.Location.X + 27 * v.getCalle() + 50 * (v.getCalle())),
                                        (label1.Bounds.Location.Y + 30 * v.getAvenida() + 50 * (v.getAvenida())), 25, 25);
                    }
                    for (int i = 0; i < 100; i++) { 
                        if(autos[i] == null){
                            autos[i] = v;
                            vehiculos[i] = label;
                            break;
                        }
                    }
                    break;
                case 2:
                    Restaurante r = new Restaurante(datos[0], Convert.ToInt32(datos[1]), Convert.ToInt32(datos[2]), Convert.ToInt32(datos[3]));
                    if (mapa[r.getAvenida() - 1, r.getCalle() - 1] != null)
                        bandera = false;
                    if (bandera) {
                        label.Image = Image.FromFile(path + "Restaurant.png");
                        label.SetBounds((label1.Bounds.Location.X + 27 * r.getCalle() + 50 * (r.getCalle() - 1)),
                                        (label1.Bounds.Location.Y + 30 * r.getAvenida() + 50 * (r.getAvenida() - 1)), 50, 50);
                        mapa[r.getAvenida() - 1, r.getCalle() - 1] = label;
                        objetos[r.getAvenida() - 1, r.getCalle() - 1] = r;
                    }
                    break;
                case 3:
                    Hospital h = new Hospital(datos[0], Convert.ToInt32(datos[1]), Convert.ToInt32(datos[2]), Convert.ToInt32(datos[3]));
                    if (mapa[h.getAvenida() - 1, h.getCalle() - 1] != null)
                        bandera = false;
                    if (bandera) {
                        label.Image = Image.FromFile(path + "Hospital.png");
                        label.SetBounds((label1.Bounds.Location.X + 27 * h.getCalle() + 50 * (h.getCalle() - 1)),
                                        (label1.Bounds.Location.Y + 30 * h.getAvenida() + 50 * (h.getAvenida() - 1)), 50, 50);
                        mapa[h.getAvenida() - 1, h.getCalle() - 1] = label;
                        objetos[h.getAvenida() - 1, h.getCalle() - 1] = h;
                    }
                    break;
                case 4:
                    Gasolinera g = new Gasolinera(datos[0], Convert.ToDouble(datos[1]), Convert.ToInt32(datos[2]), Convert.ToInt32(datos[3]));
                    if (mapa[g.getAvenida() - 1, g.getCalle() - 1] != null)
                        bandera = false;
                    if (bandera) {
                        label.Image = Image.FromFile(path + "Gas-Station.png");
                        label.SetBounds((label1.Bounds.Location.X + 27 * g.getCalle() + 50 * (g.getCalle() - 1)),
                                        (label1.Bounds.Location.Y + 30 * g.getAvenida() + 50 * (g.getAvenida() - 1)), 50, 50);
                        mapa[g.getAvenida() - 1, g.getCalle() - 1] = label;
                        objetos[g.getAvenida() - 1, g.getCalle() - 1] = g;
                    }
                    break;
                case 5:
                    Policia p = new Policia(datos[0], Convert.ToInt32(datos[1]), Convert.ToInt32(datos[2]));
                    if (mapa[p.getAvenida() - 1, p.getCalle() - 1] != null)
                        bandera = false;
                    if (bandera) {
                        label.Image = Image.FromFile(path + "Police.png");
                        label.SetBounds((label1.Bounds.Location.X + 27 * p.getCalle() + 50 * (p.getCalle() - 1)),
                                        (label1.Bounds.Location.Y + 30 * p.getAvenida() + 50 * (p.getAvenida() - 1)), 50, 50);
                        mapa[p.getAvenida() - 1, p.getCalle() - 1] = label;
                        objetos[p.getAvenida() - 1, p.getCalle() - 1] = p;
                    }
                    break;
                case 6:
                    Bombero b = new Bombero(datos[0], Convert.ToInt32(datos[1]), Convert.ToInt32(datos[2]));
                    if (mapa[b.getAvenida() - 1, b.getCalle() - 1] != null)
                        bandera = false;
                    if (bandera) {
                        label.Image = Image.FromFile(path + "Fireman.png");
                        label.SetBounds((label1.Bounds.Location.X + 27 * b.getCalle() + 50 * (b.getCalle() - 1)),
                                        (label1.Bounds.Location.Y + 30 * b.getAvenida() + 50 * (b.getAvenida() - 1)), 50, 50);
                        mapa[b.getAvenida() - 1, b.getCalle() - 1] = label;
                        objetos[b.getAvenida() - 1, b.getCalle() - 1] = b;
                    }
                    break;
            }
            if (bandera) {
                label.Click += mostrarInfo;
                panel1.Controls.Add(label);
            }
        }

        public void mostrarInfo(object sender, EventArgs e) {
            for (int fila = 0; fila < 30; fila++) {
                for (int columna = 0; columna < 30; columna++) {
                    if (mapa[fila, columna] == (sender as Label)) {
                        MessageBox.Show(objetos[fila, columna].GetType().Name);
                    }
                }
            }
            for (int i = 0; i < 100; i++) {
                if (vehiculos[i] == (sender as Label)) {
                    MessageBox.Show(autos[i].getPlaca());
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


    }
}
