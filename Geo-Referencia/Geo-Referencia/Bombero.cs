﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geo_Referencia
{
    class Bombero
    {
        private string nombre;
        private int calle;
        private int avenida;

        public Bombero(string nombre, int calle, int avenida) {
            this.nombre = nombre;
            this.calle = calle;
            this.avenida = avenida;
        }

        public string getNombre() {
            return nombre;
        }

        public int getCalle() {
            return calle;
        }

        public int getAvenida() {
            return avenida;
        }
    }
}
